#include <stdio.h>
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#include "miniz.h"
#include "stb_image.h"

#define LINE_LENGTH 8

int main(int argc, const char **argv) {
  if (argc < 4) {
    printf(
        "Asset-header: converts a file into "
        "a byte array in a C header file\n");
    printf(
        "Usage: \"asset_path.exe input_file.png "
        "OBJECT_NAME output_path.h\"\n");

    return EXIT_SUCCESS;
  }

  // read the image's data
  int width, height, channels;
  unsigned char *raw_image_data =
      stbi_load(argv[1], &width, &height, &channels, STBI_rgb);

  unsigned long raw_size = width * height * channels;

  if (!raw_image_data) {
    printf("Failed to read image file at %s\n", argv[1]);
    return EXIT_FAILURE;
  }

  // compress image data
  mz_ulong compressed_size = mz_compressBound(raw_size);
  unsigned char *buffer = malloc(compressed_size);
  int compression_status =
      mz_compress(buffer, &compressed_size, raw_image_data, raw_size);

  stbi_image_free(raw_image_data);

  if (compression_status != MZ_OK) {
    printf("Failed to compress raw image data: %s\n",
           mz_error(compression_status));
    if (buffer) {
      free(buffer);
    }
    return EXIT_FAILURE;
  }

  buffer = realloc(buffer, compressed_size);

  if (!buffer) {
    printf("Failed to read image file at %s\n", argv[1]);
  } else {
    // open/create the header file
    FILE *output_file = fopen(argv[3], "wb");
    if (!output_file) {
      perror("Could not create output file");
      stbi_image_free(buffer);
      return EXIT_FAILURE;
    }

    // write the header's prelude (include-guards, array declaration)
    fprintf(output_file, "#ifndef %s_H\n", argv[2]);
    fprintf(output_file, "#define %s_H\n\n", argv[2]);
    fprintf(output_file, "const unsigned long %s_SIZE = %lu;\n", argv[2],
            compressed_size);
    fprintf(output_file, "const unsigned long %s_UNCOMPRESSED_SIZE = %lu;\n",
            argv[2], raw_size);
    fprintf(output_file, "const int %s_IMAGE_WIDTH = %d;\n", argv[2], width);
    fprintf(output_file, "const int %s_IMAGE_HEIGHT = %d;\n", argv[2], height);
    fprintf(output_file, "const int %s_IMAGE_CHANNELS = %d;\n", argv[2],
            channels);
    fprintf(output_file, "const unsigned char %s[%lu] = {\n", argv[2],
            compressed_size);

    long bytes_written = 0;

    // write those bytes to the header file formatted as hexidecimal
    for (int i = 0; i < compressed_size; i++) {
      fprintf(output_file, "0x%02x", buffer[i]);
      if (bytes_written < compressed_size - 1) fputs(", ", output_file);
      if (bytes_written > 0 && (bytes_written + 1) % LINE_LENGTH == 0)
        fputc('\n', output_file);
      ++bytes_written;
    }

    // wrap up the declaration and include-guard
    fputs("\n};\n", output_file);
    fputs("#endif", output_file);

    fclose(output_file);

    free(buffer);
  }

  return EXIT_SUCCESS;
}
