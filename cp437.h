#ifndef CP437_H
#define CP437_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

// decompresses and returns an array of bytes consisting of
// the bitmap font's glyph texture
//
// note that the return value must be free'd by the caller
unsigned char *cp437_get_texture(unsigned long *data_size, int *width,
                                 int *height, int *channels);

// computes glyph coordinates and dimensions based on ASCII
// value of a character
void cp437_get_glyph(const unsigned char c, int *x, int *y, int *w, int *h);

void cp437_get_bounds(int *w, int *h);

#ifdef __cplusplus
}
#endif

#endif
