#include "cp437_data.h"
#include "miniz.h"

#define GLYPH_WIDTH cp437_FONT_DATA_IMAGE_WIDTH / 16
#define GLYPH_HEIGHT cp437_FONT_DATA_IMAGE_HEIGHT / 16

unsigned char *cp437_get_texture(unsigned long *data_size, int *width,
                                 int *height, int *channels) {
  unsigned char *buffer =
      malloc(cp437_FONT_DATA_UNCOMPRESSED_SIZE * sizeof(unsigned char));

  *data_size = cp437_FONT_DATA_UNCOMPRESSED_SIZE;
  int uncomp_status =
      mz_uncompress(buffer, data_size, cp437_FONT_DATA, cp437_FONT_DATA_SIZE);

  if (uncomp_status != MZ_OK) {
    printf("%d: %s\n", __LINE__, mz_error(uncomp_status));
  }

  *width = cp437_FONT_DATA_IMAGE_WIDTH;
  *height = cp437_FONT_DATA_IMAGE_HEIGHT;
  *channels = cp437_FONT_DATA_IMAGE_CHANNELS;

  return buffer;
}

void cp437_get_glyph(const unsigned char c, int *x, int *y, int *w, int *h) {
  *x = ((int)c % 16) * GLYPH_WIDTH;
  *y = ((int)c / 16) * GLYPH_HEIGHT;
  *w = GLYPH_WIDTH;
  *h = GLYPH_HEIGHT;
}

void cp437_get_bounds(int *w, int *h) {
  *w = GLYPH_WIDTH;
  *h = GLYPH_HEIGHT;
}
